# stagEZ

Jednoduchá a lehká api pro komunikaci mezi ws nad IS/STAG

## Seznam služeb

|||
|--|--|
| **AuthLogin**||
| Komentář| Používáno ke přihlášení studenta |
| REST Adresa| /Auth/Login |
| HTTP metoda| GET |
| parametry|`Username`, `Password`  |
| Vyžadováno přihlášení| Ne|
| Výstup| JSON|
| Možné Výstupové kódy| 497, 495, 406, 401, 200 |
| **AuthLogout** |
| Komentář| Používáno ke odhlášení studenta |
| REST Adresa| /Auth/Logout|
| HTTP metoda| GET |
| parametry| |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy| 401, 200 |
| **StudentGetByName** |
| Komentář| Vyhledávání studentů |
| REST Adresa| /Student/GetByName|
| HTTP metoda| GET |
| parametry|`Name`, `Surname` |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy| 406 ,401, 204, 200 |
| **StudentGetSchedule** |
| Komentář| Vyhledávání studenského rozvrhu|
| REST Adresa| /Student/GetSchedule|
| HTTP metoda| GET |
| parametry|`Oscislo`, nepovinné: `Datefrom`,`Dateto`,`Onlyupcoming`,`Nocreditterms`|
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy| 494 ,406 ,401, 204, 200 |
| **StudentGetOsCisloByStagId** |
| Komentář| Vyhledávání osobního čísla podle stagovského přihlášení|
| REST Adresa| /Student/GetOsCisloByStagId|
| HTTP metoda| GET |
| parametry|`Stagid` |
| Vyžadováno přihlášení| Ne|
| Výstup| JSON|
| Možné Výstupové kódy| 406 ,401, 204, 200 |
| **StudentGetByOsCislo** |
| Komentář| Vyhledávání studenta podle osobního čísla|
| REST Adresa| /Student/GetByOsCislo|
| HTTP metoda| GET |
| parametry|`Oscislo` |
| Vyžadováno přihlášení| Ne|
| Výstup| JSON|
| Možné Výstupové kódy| 406 ,401, 204, 200 |
| **StudentGetGrades** |
| Komentář| Získá známky aktuálního semestru přihlášeného žáka|
| REST Adresa| /Student/GetGrades|
| HTTP metoda| GET |
| parametry| |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy|  401, 204, 200 |
| **StudentGetCredits** |
| Komentář| Získá celkový počet kreditů přihlášenéhop studenta|
| REST Adresa| /Student/GetCredits|
| HTTP metoda| GET |
| parametry| |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy|  401, 204, 200 |
| **StudentGetInfo** |
| Komentář| Získá celkové info o přihlášeném uživateli|
| REST Adresa| /Student/GetByOsCislo|
| HTTP metoda| GET |
| parametry| |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy|  401, 204, 200 |
| **StudentGetAbsolvedSubjects** |
| Komentář| Získá všechny absolvované předměty|
| REST Adresa| /Student/GetAbsolvedSubjects|
| HTTP metoda| GET |
| parametry| |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy|  401, 204, 200 |
| **StudentGetTerms** |
| Komentář| Získá následující termíny zkoušek|
| REST Adresa| /Student/GetTerms|
| HTTP metoda| GET |
| parametry| |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy|  401, 204, 200 |
| **StudentGetByCourse** |
| Komentář| Získá studenty kteří jsou na zvoleném předmětu|
| REST Adresa| /Student/GetByCourse|
| HTTP metoda| GET |
| parametry|`Coursescheduleid` |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy| 406 ,401, 204, 200 |
| **StudentGetByTerm** |
| Komentář| Získá studenty kteří jsou na zvoleném termínu|
| REST Adresa| /Student/GetByTerm|
| HTTP metoda| GET |
| parametry|`Termid` |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy| 406 ,401, 204, 200 |
| **StudentSetTerm** |
| Komentář| Odhlásí či přihlásí studenta k termínu|
| REST Adresa| /Student/SetTerm|
| HTTP metoda| GET |
| parametry|`Termid`, `Termisreg` |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy| 493 ,410 ,401, 204, 200 |
| **TeacherGetByName** |
| Komentář| Vyhledávání vyučujících|
| REST Adresa| /Teacher/GetByName|
| HTTP metoda| GET |
| parametry|`Name`, `Surname` |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy| 406 ,401, 204, 200 |
| **TeacherGetSchedule** |
| Komentář| Vyhledávání učitelského rozvrhu|
| REST Adresa| /Teacher/GetSchedule|
| HTTP metoda| GET |
| parametry|`Teacherid`, nepovinné: `Datefrom`,`Dateto`,`Onlyupcoming`,`Nocreditterms` |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy| 494 ,406 ,401, 204, 200 |
| **RoomGetFreeRoomsInBuilding** |
| Komentář| Vyhledávání prázdných učeben podle zadané budovy|
| REST Adresa| /Room/GetFreeRoomsInBuilding|
| HTTP metoda| GET |
| parametry|`Schedulepos`, `Datefrom`, `Buildings` |
| Vyžadováno přihlášení| Ano|
| Výstup| JSON|
| Možné Výstupové kódy| 406 ,401, 200 |

## Seznam výstupových kódů

|Kód|význam|
|--|--|
|497|Vynucené odhlášení|
|495|Odhlášení selhalo|
|494|Špatný formát datumu|
|493|Špatný Id termínu|
|410|Termín už je zapsán / odepsán
|406|parametr nenalezten|
|404|Špatná kategorie či metoda|
|401|Uživatel nepřihlášen / Zamítnuté přihlášení k WS|
|204|Nic nenalezeno|
|200|OK|

## Seznam výstupových objektů

Veškeré GET Requesty vypadají například takto: ```www.example.com/Auth/Login``` nebo ```www.example.com/Student/GetSchedule```

```
+ GET /Auth/Login   -Parameters>   Username=st***** Password=*****
    =>{
        "stagUserName": "st*****",
        "stagUserRole": "ST",
        "stagUserOsCislo": "F*****",
        "stagUserInfo": {
            "stagUserName": "st*****",
            "stagUserOsCislo": "F*****",
            "stagUserFirstName": "Jan",
            "stagUserLastName": "ĎURĎÁK",
            "stagUserField": "Aplikovaná informatika",
            "stagUserFaculty": "PRF",
            "stagUserCurrentGrade": 1
        }
    }

+ GET /Auth/Logout     -Parameters>   [EMPTY]
    => (200 OK)

+ GET /Student/GetByName   -Parameters>   Name=Jan Surname=Durdak
    =>[
        {
            "stagUserName": "st*****",
            "stagUserOsCislo": "F*****",
            "stagUserFirstName": "Jan",
            "stagUserLastName": "ĎURĎÁK",
            "stagUserField": "Aplikovaná informatika",
            "stagUserFaculty": "PRF",
            "stagUserCurrentGrade": 1
        }
    ]

+ GET /Student/GetSchedule   -Parameters>   Oscislo=F*****    (Datefrom=7.5.2018 Dateto=13.5.2018) 
    =>[
        {
            "ScheduleId": "P232",
            "ScheduleName": "Vybrané partie z matematiky",
            "ScheduleType": "Př",
            "ScheduleDate": "10.5.2018",
            "ScheduleYear": "2017",
            "ScheduleFrom": "08:00",
            "ScheduleTo": "08:50",
            "ScheduleTeacher": {
                "stagUserTeacherId": 261,
                "stagUserFirstName": "Petr",
                "stagUserLastName": "Eisenmann"
            },
            "ScheduleDepartment": "KMA",
            "ScheduleBuilding": "CN",
            "ScheduleRoom": "617"
        }
    ]

+ GET /Student/GetOsCisloByStagId   -Parameters>   Stagid=st*****
    =>{
        "stagUserOsCislo": "F*****"
    }

+ GET /Student/GetByOsCislo   -Parameters>   Oscislo=F*****
    =>{
        "stagUserName": "st*****",
        "stagUserOsCislo": "F*****",
        "stagUserFirstName": "Jan",
        "stagUserLastName": "ĎURĎÁK",
        "stagUserField": "Aplikovaná informatika",
        "stagUserFaculty": "PRF",
        "stagUserCurrentGrade": 1
    }

+ GET /Student/GetGrades -Parameters>
    =>[
        {
            "GradeCourse": {
                "CourseFaculty": "CJP",
                "CourseId": "0286",
                "CourseName": "Angličtina A I",
                "CoursePoints": 2,
                "CourseHasExam": false,
                "CourseFinished": true
            },
            "GradeAcredation": null,
            "GradeExam": {
                "GradeByNumber": false,
                "GradeStatus": true,
                "GradeAttempt": "1",
                "GradeAttemptDate": "10.05.2018",
                "GradeTeacher": {
                    "stagUserTeacherId": 5754,
                    "stagUserFirstName": "Vladimír",
                    "stagUserLastName": "Lorenc"
                }
            }
        }
    ]

+ GET /Student/GetCredits -Parameters>
    =>{
        "StudentCredits": 41
    }

+ GET /Student/GetInfo -Parameters>
    =>{
        "StudentFirstName": "Jan",
        "StudentLastName": "ĎURĎÁK",
        "StudentTitleBefore": null,
        "StudentTitleAfter": null,
        "StudentUserName": "st*****",
        "StudentOsCislo": "F*****",
        "StudentField": "Aplikovaná informatika",
        "StudentFaculty": "PRF",
        "StudentCurrentYear": 1,
        "StudentStudyState": true,
        "StudentCredits": 41
    }

+ GET /Student/GetAbsolvedSubjects -Parameters>
    =>[
        {
            "CourseFaculty": "KI",
            "CourseId": "AHW",
            "CourseName": "Praktické aplikace hardwaru",
            "CourseSemester": "ZS",
            "CourseYear": "2017",
            "CourseDateAbsolved": "29.1.2018",
            "CourseAbsolved": true,
            "CourseGrade": "S",
            "CourseCreditCount": 1
        }
    ]

+ GET /Student/GetTerms -Parameters>
    =>[
        {
            "ScheduleDepartment": "KMA",
            "ScheduleId": "P231",
            "ScheduleName": "Lineární algebra a geometrie",
            "ScheduleType": "Zkouška",
            "ScheduleDate": "5.9.2018",
            "ScheduleYear": "2017",
            "ScheduleDay": "St",
            "ScheduleFrom": "08:30",
            "ScheduleTo": "13:00",
            "ScheduleTeacher": {
                "stagUserTeacherId": 425,
                "stagUserFirstName": "Magdalena",
                "stagUserLastName": "Krátká"
            },
            "ScheduleBuilding": "CN",
            "ScheduleRoom": "617",
            "ScheduleTerm": {
                "TermId": 343652,
                "TermLimit": "null",
                "TermStudentCount": "3",
                "TermRegistered": true,
                "TermCanUnregister": true,
                "TermRegistrationDeadline": "4.9.2018 8:30",
                "TermUnregistrationDeadline": "4.9.2018 7:30"
            }
        }
    ]

+ GET /Student/SetTerm    -Parameters>   Termid=343652 Termisreg=true (True for registration, False for Unregister)
    => (200 OK)


+ GET /Teacher/GetByName   -Parameters>   Name=Jiri Surname=Skvor
    =>[
        {
            "stagUserTeacherId": 2220,
            "stagUserFirstName": "Jiří",
            "stagUserLastName": "Škvor"
        }
    ]

+ GET /Teacher/GetSchedule   -Parameters>   Teacherid=261   (Datefrom=10.5.2018 Dateto=27.5.2018) 
    =>[
        {
            "ScheduleId": "P225",
            "ScheduleName": "Metody řešení matematických úloh",
            "ScheduleType": "Cv",
            "ScheduleDate": "14.5.2018",
            "ScheduleYear": "2017",
            "ScheduleDay": "Po",
            "ScheduleFrom": "10:00",
            "ScheduleTo": "12:50",
            "ScheduleTeacher": {
                "stagUserTeacherId": 261,
                "stagUserFirstName": "Petr",
                "stagUserLastName": "Eisenmann"
            },
            "ScheduleDepartment": "KMA",
            "ScheduleBuilding": "CN",
            "ScheduleRoom": "617"
        }
    ]

+ GET /Room/GetFreeRoomsInBuilding   -Parameters>   Schedulepos=6 Datefrom=7.5.2018 Buildings=%5B%22CN%22%5D  (url encoded ["CN"] )
    =>[
        {
            "RoomFaculty": "PF",
            "RoomBuilding": "CN",
            "RoomBuildingAdress": "České mládeže 8, PF - budova kateder B, Ústí nad Labem",
            "RoomNumber": "530",
            "RoomCapacity": 15,
            "RoomFloor": "5",
            "RoomFree": false
        },
        {
            "RoomFaculty": "PF",
            "RoomBuilding": "CN",
            "RoomBuildingAdress": "České mládeže 8, PF - budova kateder B, Ústí nad Labem",
            "RoomNumber": "113",
            "RoomCapacity": 25,
            "RoomFloor": "1",
            "RoomFree": true
        }
    ]



```
