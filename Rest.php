<?php 

require_once 'vendor/autoload.php';
session_start();

use Goutte\Client;

class Rest {
    private $request = array();
    private $code = 200;
    private $defaultRequestUrl = "https://ws.ujep.cz/ws/services/rest/";

    private $allowInvalidTimes = false;
    
    public function __construct(){
        $this->getRequestType();
    }

    public function processRequest(){
        $this->getValidMethod();
    }

    private function getRequestMethod(){
		return $_SERVER['REQUEST_METHOD'];
	}

    private function getRequestType(){
		switch($this->getRequestMethod()){
			case "GET":
                $this->request = $this->getData($_GET);
            break;
			default: 
                $this->createResponse(406,'invalid request type');
			break;
		}
    }

    private function getData($data){
        $cleanData = array();
        if(is_array($data)){
            foreach($data as $k => $v){
                $cleanData[$k] = $this->getData($v);
            }
        }else{
            if(get_magic_quotes_gpc()){
                $data = trim(stripslashes($data));
            }
            $data = strip_tags($data);
            $cleanData = trim($data);
        }
        return $cleanData;
    }
    
    private function createResponse($status,$data){
		$this->code = ($status)?$status:200;
		header("HTTP/1.1 ".$this->code." ".$this->getStatusMessage());
        header("Content-Type:application/json; charset=utf-8");
        echo $data;
        exit;
    }
    
    private function getStatusMessage(){
        $statusCodes = array(
            //? BASE STATUS CODES 100 -> 505
            100 => 'Continue', 101 => 'Switching Protocols', 200 => 'OK', 201 => 'Created', 202 => 'Accepted', 203 => 'Non-Authoritative Information', 204 => 'No Content', 205 => 'Reset Content', 206 => 'Partial Content', 300 => 'Multiple Choices', 301 => 'Moved Permanently', 302 => 'Found', 303 => 'See Other', 304 => 'Not Modified', 305 => 'Use Proxy', 306 => '(Unused)', 307 => 'Temporary Redirect', 400 => 'Bad Request', 401 => 'Unauthorized', 402 => 'Payment Required', 403 => 'Forbidden', 404 => 'Not Found', 405 => 'Method Not Allowed', 406 => 'Not Acceptable', 407 => 'Proxy Authentication Required', 408 => 'Request Timeout', 409 => 'Conflict', 410 => 'Gone', 411 => 'Length Required', 412 => 'Precondition Failed', 413 => 'Request Entity Too Large', 414 => 'Request-URI Too Long', 415 => 'Unsupported Media Type', 416 => 'Requested Range Not Satisfiable', 417 => 'Expectation Failed', 500 => 'Internal Server Error', 501 => 'Not Implemented', 502 => 'Bad Gateway', 503 => 'Service Unavailable', 504 => 'Gateway Timeout', 505 => 'HTTP Version Not Supported',
            //? CUSTOM STATUS CODES
            499 => 'Session expired',
            498 => 'Already logged in',
            497 => 'Forced logout',
            496 => 'OsCislo not found',
            495 => 'Logout failed',
            494 => 'Wrong date format',
            493 => 'Invalid term id'

        );

        return ($status[$this->code])?$status[$this->code]:$status[500];
    }
    
    private function getRequestHeaders() {
		$headers = array();
		foreach($_SERVER as $key => $value) {
			if (substr($key, 0, 5) <> 'HTTP_') {
				continue;
			}
			$header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
			$headers[$header] = $value;
		}
		return $headers;
    }
    
    private function getValidMethod(){
        $cats = array(
            'Auth' => array(
                'Login' => 1,
                'Logout' => 2
            ),
            'Student' => array( 
                'GetByName' => 1,
                'GetSchedule' => 2,
                'GetOsCisloByStagId' => 3,
                'GetByOsCislo' => 4,
                'GetGrades' => 5,
                'GetCredits' => 6,
                'GetInfo' => 7,
                'GetAbsolvedSubjects' => 8,
                'GetTerms' => 9,
                'SetTerm' => 10,
                'GetByCourse' => 11,
                'GetByTerm' => 12
            ),
            'Teacher' => array(
                'GetByName' => 1,
                'GetSchedule' => 2,
            ),
            'Room' => array(
                'GetFreeRoomsInBuilding' => 1
            ),
            'Courses' => array(
                'GetByFaculty' => 1,
                'GetSubjectsByCourseId' => 2,
                'GetSubjectById' => 3,
                'GetSubjectByBlock' => 4,
                'GetSubjectByBlockFull' => 5,
            ),
            'Programs' => array(
                'GetStudyPrograms' => 1,
                'GetStudyProgramFields' => 2,
                'GetFieldPlans' => 3,
                'GetPlanSegments' => 4,
                'GetSegmentBlocks' => 5,
            ),
            'Hack' => array(
                'DecinRepresentativeVoting' => 1,
                'DecinGetFiles' => 2,
                'DecinGetVotesById' => 3,
            ),
        );

        if($cats[$this->request['category']]){
            if($cats[$this->request['category']][$this->request['method']]){
                $mn = $this->request['category'].$this->request['method'];
                if(method_exists($this,$mn)){
                    $this->$mn();
                }else{
                    $this->createResponse(404,'method does not exist');
                }
            }else{
                $this->createResponse(404,'Invalid method');
            }
        }else{
            $this->createResponse(404,'Invalid category');
        }

    }

    //? Auth methods

    private function AuthLogin(){
        $canLogin = $this->canLogIn();

        if($canLogin){
            $rData = $this->requireRequestParams(array("Username","Password"));

            Unirest\Request::auth($rData->Username,$rData->Password);
            $url = "https://ws.ujep.cz/ws/login?originalURL=".urlencode("http://stag.honzad.cz")."&basic=1";
            $headersArray = array('Accept' => 'application/json');
		    $response = Unirest\Request::get($url,$headersArray);
            $headers = $response->headers;

            $status = intval(end(explode(" ",$headers["0"])));
            if($status != 401){
                parse_str(parse_url($headers["Location"])['query'], $query);
                if(is_array($headers["Set-Cookie"])){
                    $cstr = "";
                    $retData = array();

                    foreach ($headers["Set-Cookie"] as $value) {
                        $evalue = explode("=", substr( explode(" ",$value) [0],0,-1));
                        $cstr .= $evalue[0]."=".$evalue[1]."; ";
                    }

                    $retData["stagUserName"] = $query["stagUserName"];
                    $retData["stagUserRole"] = $query["stagUserRole"];

                    $_SESSION["stagUserTicket"] = $query["stagUserTicket"];
                    $_SESSION["stagUserName"] = $query["stagUserName"];
                    $_SESSION["stagUserCookie"] = $cstr;
                    $_SESSION['stagUserCookieExpire'] = time() + (60 * 60);
                    
                    $oc = $this->StudentGetOsCisloByStagId($query["stagUserName"]);
                    if($oc != null){
                        if($oc != -1){
                            $retData["stagUserOsCislo"] = $oc;

                            $_SESSION["stagUserOsCislo"] = $oc;

                            $uis = $this->StudentGetByOsCislo($oc);
                            if($uis != -1){
                                $retData["stagUserInfo"] = $uis;

                                $this->createResponse(200,$this->toJson($retData));
                            }else{
                                // Not found -> Will not happen in this case, becouse we sucessfully logged in
                            }
                        }else{
                            // Not found -> Will not happen in this case, becouse we sucessfully logged in
                        }
                    }else{
                        // Unauthorized -> Will not happen, it will go through without login
                    }
                }else{
                    $alr = $this->AuthLogout(true,true);
                    if ($alr === 0) { // Put switch back for more info return?
                        $this->createResponse(497,'');
                    } else {
                        $this->createResponse(495,'');
                    }
                }
            }else{
                $this->createResponse(401,'what');
            }

        }
    }

    private function AuthLogout($force = false, $external = false){
        $canLogout = $this->canLogOut($force);

        if($canLogout){
            Unirest\Request::cookie($_SESSION['stagUserCookie']);
            $url = "https://ws.ujep.cz/ws/login?logout=1";
            $headersArray = array('Accept' => 'application/json');
            $response = Unirest\Request::get($url,$headersArray);
            $headers = $response->headers;

            $status = intval(end(explode(" ",$headers["0"])));
            if($status != 401){
                session_unset(); 
			    session_destroy();
                if($external){
                    return 0;
                }else{
                    $this->createResponse(200,'');
                }
            }else{
                if($external){
                    return -3;
                }else{
                    $this->createResponse(401,'');
                }
            }
        } else {
            if ($external){
                return -2;
            } else {
                $this->createResponse(401,'');
            }
        }
    }

    //? Student methods

    private function StudentGetByName(){
        $this->requireUserSession();
        $rData = $this->requireRequestParams(array("Name","Surname"));

        $fnm = urldecode($rData->Name)."%";
        $lnm = urldecode($rData->Surname)."%";

        $urlData = array(
            "prijmeni" => urlencode($lnm),
            "jmeno" => urlencode($fnm),
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."student/najdiStudentyPodleJmena?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
        $response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;
        
        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();
            $sArr = $body[0]->student;

            if(sizeof($sArr) > 0){
                foreach ($sArr as $mst) {
                    $rs = array();

                    $rs["stagUserName"] = 	        $mst->userName;         
                    $rs["stagUserOsCislo"] = 	    $mst->osCislo;        
                    $rs["stagUserFirstName"] = 	    $mst->jmeno;         
                    $rs["stagUserLastName"] = 	    $mst->prijmeni;        
                    $rs["stagUserField"] = 	        $mst->nazevSp;    
                    $rs["stagUserFaculty"] =        $mst->fakultaSp;
                    $rs["stagUserCurrentGrade"] =   intval($mst->rocnik); 

                    array_push($retData,$rs);
                }
                
                $this->createResponse(200,$this->toJson($retData));
            }else{
                $this->createResponse(204,'');
            }
        }else{
            $this->createResponse(401,'');
        }
    }

    private function StudentGetSchedule(){ 
        $this->requireUserSession();
        $rData = $this->requireRequestParams(array("Oscislo"));

        $OnlyUpcoming = ($this->existsAndValue("Onlyupcoming")[0] && $this->existsAndValue("Onlyupcoming")[1] === "true");
        $NoCreditTerms = (!$OnlyUpcoming && $this->existsAndValue("Nocreditterms")[0] && $this->existsAndValue("Nocreditterms")[1] === "true");
        $isDateFunc = (!$NoCreditTerms && !$OnlyUpcoming && $this->existsAndValue("Datefrom")[0] && $this->existsAndValue("Dateto")[0]);
        

        $urlData = array(
            "osCislo" => $rData->Oscislo,
            "stagUser" => $_SESSION["stagUserOsCislo"],
            "semestr" => $this->getCurrentSemester(),
            "rok" => date("Y") - 1,
            "jenRozvrhoveAkce" => ($NoCreditTerms ? "true" : "false"),
            "jenBudouciAkce" => ($OnlyUpcoming ? "true" : "false")
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        if($isDateFunc){
            $dFrom = $this->request["Datefrom"];
            $dTo = $this->request["Dateto"];
            if(DateTime::createFromFormat('d.m.Y',$dFrom) !== false){
                if(DateTime::createFromFormat('d.m.Y',$dTo) !== false){
                    $urlData["datumOd"] = $dFrom;
                    $urlData["datumDo"] = $dTo;
                }else{
                    $this->createResponse(494,'');
                }
            }else{
                $this->createResponse(494,'');
            }
        }

        $url = $this->defaultRequestUrl."rozvrhy/getRozvrhByStudent?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();
            $sArr = $body[0]->rozvrhovaAkce;

            if(sizeof($sArr) > 0){
                foreach ($sArr as $sch) {
                    $rs = array();

                    $rs["CourseScheduleId"] = $sch->roakIdno;

                    $rs["ScheduleId"]   =       $sch->predmet;
                    $rs["ScheduleName"] =       $sch->nazev;
                    $rs["ScheduleType"] =       $sch->typAkceZkr;

                    if($sch->datum->value == null || $sch->datum->value == "null"){
                        $rs["ScheduleDate"] =       $this->getDateByDay($sch->denZkr);
                        $rs["ScheduleStatic"] =       true;
                    }else{
                        $rs["ScheduleDate"] =       $sch->datum->value;
                    }

                    if(!$isDateFunc){
                        if($sch->tydenZkr == "J"){
                            continue;
                        }
                    }

                    $rs["ScheduleYear"] =       $sch->rok;

                    if($sch->denZkr == null || $sch->denZkr == "null"){
                        $rs["ScheduleDay"] =        $this->getDayByDate($rs["ScheduleDate"]);
                    }else{
                        $rs["ScheduleDay"] =        $sch->denZkr;
                    }

                    if($sch->hodinaSkutOd->value == "00:00"){
                        if($this->allowInvalidTimes){
                            $rs["ScheduleCourseEmptyTimeData"] = true;
                            $rs["ScheduleFrom"] = "07:00";
                            $rs["ScheduleTo"]   = "08:00";
                        }else{
                            continue;
                        }
                    }else{
                        $rs["ScheduleFrom"] =       $sch->hodinaSkutOd->value;
                        $rs["ScheduleTo"]   =       $sch->hodinaSkutDo->value;
                    }

                    $tch = array();
                    $tch["stagUserTeacherId"] = $sch->ucitel->ucitIdno; 
				    $tch["stagUserFirstName"] = $sch->ucitel->jmeno;
                    $tch["stagUserLastName"] =  $sch->ucitel->prijmeni;
                    $rs["ScheduleTeacher"]   =  $tch;

                    $rs["ScheduleDepartment"] = $sch->katedra;

                    if($sch->budova == null || $sch->budova == "null"){

                    }

					$rs["ScheduleBuilding"]  =  $sch->budova;
					$rs["ScheduleRoom"] =       $sch->mistnost;

                    array_push($retData,$rs);
                }
                
                $this->createResponse(200,$this->toJson($retData));
            }else{
                $this->createResponse(204,$this->toJson($retData));
            }

        }else{
            $this->createResponse(401,'');
        }
    }

    private function StudentGetOsCisloByStagId($id = null){
        $external = false;
        if($id == null){
            $rData = $this->requireRequestParams(array("Stagid"));
            $id = $rData->Stagid;
        }else{
            $external = true;
        }

        $url = "https://ws.ujep.cz/ws/services/rest/users/getOsobniCislaByExternalLogin?login=".$id."&outputFormat=json";
		$headersArray = array('Accept' => 'application/json');

        $response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;
        
        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();
            $osArr = $body[0]->osCislo;
            if(sizeof($osArr) > 0){
                if($external){
                    return $osArr[0];
                }else{
                    $retData["stagUserOsCislo"] = $osArr[0];
                    $this->createResponse(200,$this->toJson($retData));
                }
			}else{
                if($external){
                    return -1;
                }else{
                    $retData["stagUserOsCislo"] = null;
                    $this->createResponse(204,$this->toJson($retData));
                }
			}
        }else{
            if($external){
                return null;
            }else{
                $this->createResponse(401,'');
            }
        }
    }

    private function StudentGetByOsCislo($oc = null){
        $this->requireUserSession();

        $external = false;
        if($oc == null){
            $rData = $this->requireRequestParams(array("Oscislo"));
            $oc = $rData->Oscislo;
        }else{
            $external = true;
        }

        $eData = $this->existsAndValue("Oscislo");

        $urlData = array(
            "osCislo" => $external ? $oc : $eData[1],
            "stagUser" => $external ? $oc : $eData[1]
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."student/getStudentInfo?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
        $response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;
        
        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            if(sizeof($body) > 0){
                $mst = $body[0];

                $rs = array();
                $rs["stagUserName"] = 	        $mst->userName;         
                $rs["stagUserOsCislo"] = 	    $mst->osCislo;        
                $rs["stagUserFirstName"] = 	    $mst->jmeno;         
                $rs["stagUserLastName"] = 	    $mst->prijmeni;        
                $rs["stagUserField"] = 	        $mst->nazevSp;    
                $rs["stagUserFaculty"] =        $mst->fakultaSp;
                $rs["stagUserCurrentGrade"] =   intval($mst->rocnik); 

                if($external){
                    return $rs;
                }else{
                    $this->createResponse(200,$this->toJson($rs));
                }
			}else{
                if($external){
                    return -1;
                }else{
                    $this->createResponse(204,'empty array');
                }
			}
        }else{
            if($external){
                return -1;
            }else{
                $this->createResponse(401,'');
            }
        }


    }

    private function StudentGetGrades(){
        $this->requireUserSession();

        $urlData = array(
            "osCislo" => $_SESSION["stagUserOsCislo"],
            "stagUser" => $_SESSION["stagUserOsCislo"],
            "semestr" => $this->getCurrentSemester()
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."znamky/getZnamkyByStudent?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();
            $zArr = $body[0]->student_na_predmetu;

            if(sizeof($zArr) > 0){
                foreach ($zArr as $grd) {
                    $rs = array();

                    $rs["GradeCourse"] = $this->CourseGetInfo($grd->katedra,$grd->zkratka);

                    if($grd->zppzk_datum != null){
                        $acreArr = array();
                        $acreArr["GradeByNumber"] = false;
                        if($grd->zppzk_hodnoceni != ""){
                            if($grd->zppzk_hodnoceni == "S"){
                                $acreArr["GradeStatus"] = true;
                            }else{
                                $acreArr["GradeStatus"] = false;
                            }
                        }else{
                            $acreArr["GradeStatus"] = false;
                        }

                        $acreArr["GradeAttempt"] = intval($grd->zppzk_pokus);

                        if($grd->zppzk_datum != ""){
                            $acreArr["GradeAttemptDate"] = $grd->zppzk_datum;
                        }else{
                            $acreArr["GradeAttemptDate"] = null;
                        }

                        if($grd->zppzk_ucit_idno != ""){
                            $getb = array();
                            $getb["stagUserTeacherId"] = intval($grd->zppzk_ucit_idno);
                            $getb["stagUserFirstName"] = explode(" ", $grd->zppzk_ucit_jmeno)[1];
                            $getb["stagUserLastName"] = explode(" ", $grd->zppzk_ucit_jmeno)[0];
        
                            $acreArr["GradeTeacher"] = $getb;
                        }else{
                            $acreArr["GradeTeacher"] = null;
                        }

                        $rs["GradeAcredation"] = $acreArr;
                    }else{
                        $rs["GradeAcredation"] = null;
                    }

                    $examArr = array();
                    $examByNumber = ($grd->zk_typ_hodnoceni == "Slovně" ? false : true);
                    $examArr["GradeByNumber"] = $examByNumber;
                    if($examByNumber){
                        if($grd->zk_hodnoceni != ""){
                            $examArr["GradeStatus"] = intval($grd->zk_hodnoceni);
                        }else{
                            $examArr["GradeStatus"] = 0;
                        }
                    }else{
                        if($grd->zk_hodnoceni != ""){
                            if($grd->zk_hodnoceni == "S"){
                                $examArr["GradeStatus"] = true;
                            }else{
                                $examArr["GradeStatus"] = false;
                            }
                        }else{
                            $examArr["GradeStatus"] = false;
                        }
                    }
                    $examArr["GradeAttempt"] = intval($grd->zk_pokus);

                    if($grd->zk_datum != ""){
                        $examArr["GradeAttemptDate"] = $grd->zk_datum;
                    }else{
                        $examArr["GradeAttemptDate"] = null;
                    }
                    if($grd->zk_ucit_idno != ""){
                        $geta = array();
                        $geta["stagUserTeacherId"] = intval($grd->zk_ucit_idno);
                        $geta["stagUserFirstName"] = explode(" ", $grd->zk_ucit_jmeno)[1];
                        $geta["stagUserLastName"] = explode(" ", $grd->zk_ucit_jmeno)[0];
    
                        $examArr["GradeTeacher"] = $geta;
                    }else{
                        $examArr["GradeTeacher"] = null;
                    }

                    $rs["GradeExam"] = $examArr;

                    if($rs["GradeCourse"]->CourseHasExam){
                        if($grd->zppzk_hodnoceni != ""){
                            if($grd->zppzk_hodnoceni == "S"){
                                if($grd->zk_pokus != "0"){
                                    if($grd->zk_typ_hodnoceni == "Slovně"){
                                        if($grd->zk_hodnoceni == "S"){
                                            $rs["GradeCourse"]["CourseFinished"] = true;
                                        }else{
                                            $rs["GradeCourse"]["CourseFinished"] = false;
                                        }
                                    }else{
                                        if(intval($grd->zk_hodnoceni) < 4){
                                            $rs["GradeCourse"]["CourseFinished"] = true;
                                        }else{
                                            $rs["GradeCourse"]["CourseFinished"] = false;
                                        }
                                    }
                                }else{
                                    $rs["GradeCourse"]["CourseFinished"] = false;
                                }
                            }else{
                                $rs["GradeCourse"]["CourseFinished"] = false;
                            }
                        }else{
                            $rs["GradeCourse"]["CourseFinished"] = false;
                        }
                    }else{ // Ended by Acredation
                        if($grd->zk_pokus != "0"){
                            if($grd->zk_typ_hodnoceni == "Slovně"){
                                if($grd->zk_hodnoceni == "S"){
                                    $rs["GradeCourse"]["CourseFinished"] = true;
                                }else{
                                    $rs["GradeCourse"]["CourseFinished"] = false;
                                }
                            }else{
                                if(intval($grd->zk_hodnoceni) < 4){
                                    $rs["GradeCourse"]["CourseFinished"] = true;
                                }else{
                                    $rs["GradeCourse"]["CourseFinished"] = false;
                                }
                            }
                        }else{
                            $rs["GradeCourse"]["CourseFinished"] = false;
                        }
                    }

                    array_push($retData,$rs);
                }
                $this->createResponse(200,$this->toJson($retData));
            }else{
                $this->createResponse(204,$this->toJson($retData));
            }
        }else{
            $this->createResponse(401,'');
        }

    }

    private function StudentGetCredits($outer = false){
        if(!$this->isUserLogged()){
            if($outer){
                return -1;
            }else{
                $this->createResponse(401,'');
            } 
        }

        $urlData = array(
            "stagUser" => $_SESSION["stagUserOsCislo"]
        );

        $url = $this->defaultRequestUrl."student/getStudentPredmetyAbsolvoval?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();
            $zArr = $body[0]->predmetAbsolvoval;

            $pcount = 0;
            if(sizeof($zArr) > 0){
                foreach ($zArr as $pr) {
                    if($pr->absolvoval == "A"){
                        $pcount += (intval($pr->pocetKreditu));
                    }
                }
                $retData["StudentCredits"] = $pcount;

                if($outer){
                    return $retData;
                }else{
                    $this->createResponse(200,$this->toJson($retData));
                } 
            }else{
                if($outer){
                    return -1;
                }else{
                    $this->createResponse(204,$this->toJson($retData));
                } 
            }
        }else{
            if($outer){
                return -1;
            }else{
                $this->createResponse(401,'');
            } 
        }

    }

    private function StudentGetInfo(){
        $this->requireUserSession();

        $urlData = array(
            "osCislo" => $_SESSION["stagUserOsCislo"],
            "stagUser" => $_SESSION["stagUserOsCislo"]
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."student/getStudentInfo?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();

            if(sizeof($body) > 0){
                $mst = $body[0];
                if(!empty($mst)){
                    $currentCredits = $this->StudentGetCredits(true);

                    if($currentCredits != -1){
                        $retData["StudentFirstName"] = $mst->jmeno;
                        $retData["StudentLastName"] = $mst->prijmeni;

                        $retData["StudentTitleBefore"] = $mst->titulPred;
                        $retData["StudentTitleAfter"] = $mst->titulZa;

                        $retData["StudentUserName"] = $mst->userName;         
                        $retData["StudentOsCislo"] = $mst->osCislo; 
                        
                        $retData["StudentField"] = $mst->nazevSp;         
                        $retData["StudentFaculty"] = $mst->fakultaSp; 
                        $retData["StudentCurrentYear"] = intval($mst->rocnik);         
                        $retData["StudentStudyState"] = ($mst->stav == "S" ? true : false); 

                
                        $this->createResponse(200,$this->toJson(array_merge($retData,$currentCredits)));
                    }else{
                        $this->createResponse(401,'');
                    }
                }else{
                    $this->createResponse(204,$this->toJson($retData));
                }
            }else{
                $this->createResponse(204,$this->toJson($retData));
            }
        }else{
            $this->createResponse(401,'');
        }

    }

    private function StudentGetAbsolvedSubjects(){
        $this->requireUserSession();

        $urlData = array(
            "stagUser" => $_SESSION["stagUserOsCislo"]
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."student/getStudentPredmetyAbsolvoval?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();
            $pArr = $body[0]->predmetAbsolvoval;

            if(sizeof($pArr) > 0){
                foreach ($pArr as $pr) {
                    $rs = array();

                    $rs["CourseFaculty"] = $pr->katedra;
                    $rs["CourseId"] = $pr->zkratka;
                    $rs["CourseName"] = $pr->nazevPredmetu;
                    $rs["CourseSemester"] = $pr->semestr;
                    $rs["CourseYear"] = $pr->rok;
                    $rs["CourseDateAbsolved"] = $pr->datum->value;
                    $rs["CourseAbsolved"] = ($pr->absolvoval == "A"? true : false);
                    $rs["CourseGrade"] = $pr->znamka;
                    $rs["CourseCreditCount"] = intval($pr->pocetKreditu);
                    
                    array_push($retData,$rs);
                }
                $this->createResponse(200,$this->toJson($retData));
            }else{
                $this->createResponse(204,$this->toJson($retData));
            }
        }else{
            $this->createResponse(401,'');
        }
    }

    private function StudentGetTerms(){
        $this->requireUserSession();

        $urlData = array(
            "osCislo" => $_SESSION["stagUserOsCislo"],
            "stagUser" => $_SESSION["stagUserOsCislo"]
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."terminy/getTerminyProStudenta?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();
            $tArr = $body[0]->termin;

            if(sizeof($tArr) > 0){
                foreach ($tArr as $term) {
                    $rs = array();
                    $courseObj = $this->CourseGetInfo($term->katedra,$term->predmet);

                    $rs["ScheduleDepartment"] = $term->katedra;
                    $rs["ScheduleId"]   =       $term->predmet;
                    $rs["ScheduleName"] =       $courseObj["CourseName"];
                    $rs["ScheduleType"] =       $term->typTerminu;

                    $rs["ScheduleDate"] =       $term->datum->value;
                    $rs["ScheduleYear"] =       $term->rok;
                    $rs["ScheduleDay"] =        $this->getDayByDate($rs["ScheduleDate"]);
                    $rs["ScheduleFrom"] =       substr($term->casOd,0,-3);
                    $rs["ScheduleTo"]   =       substr($term->casDo,0,-3);
 
                    $tch = array();
                    $tch["stagUserTeacherId"] = $term->ucitel->ucitIdno; 
				    $tch["stagUserFirstName"] = $term->ucitel->jmeno;
                    $tch["stagUserLastName"] =  $term->ucitel->prijmeni;
                    $rs["ScheduleTeacher"]   =  $tch;

					$rs["ScheduleBuilding"]  =  $term->budova;
                    $rs["ScheduleRoom"] =       $term->mistnost;
                    
                    $termObject = array();

                    $termObject["TermId"] = $term->termIdno;

                    if($term->limit == "null" || $term->limit == null){
                        $termObject["TermLimit"] = -1;
                    }else{
                        $termObject["TermLimit"] = intval($term->limit);
                    }

                    $termObject["TermStudentCount"] = intval($term->obsazeni);
                    $termObject["TermRegistered"] = $term->zapsan;
                    $termObject["TermCanUnregister"] = $term->lzeZapsatOdepsat;
                    $termObject["TermRegistrationDeadline"] = $term->deadlineDatumPrihlaseni->value;
                    $termObject["TermUnregistrationDeadline"] = $term->deadlineDatumOdhlaseni->value;

                    $rs["ScheduleTerm"] = $termObject;

                    array_push($retData,$rs);
                }
                $this->createResponse(200,$this->toJson($retData));
            }else{
                $this->createResponse(204,$this->toJson($retData));
            }
        }else{
            $this->createResponse(401,'');
        }
    }

    private function StudentSetTerm(){
        $this->requireUserSession();

        $rData = $this->requireRequestParams(array("Termid", "Termisreg"));
        $isRegister = filter_var($rData->Termisreg, FILTER_VALIDATE_BOOLEAN);
        
        $urlData = array(
            "termIdno" => $rData->Termid,
            "osCislo" => $_SESSION["stagUserOsCislo"],
            "stagUser" => $_SESSION["stagUserOsCislo"]
        );

        $url = ($isRegister == true ? ( $this->defaultRequestUrl."terminy/zapisStudentaNaTermin?outputFormat=json"): ( $this->defaultRequestUrl."terminy/odhlasStudentaZTerminu?outputFormat=json"));

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $btst = print_r($body, 1);
            if(strpos($btst,'IllegalArgumentException') !== true){
                if($body[0] === "OK"){
                    $this->createResponse(200,'');
                }else{
                    $this->createResponse(410,'');
                }
            }else{
                $this->createResponse(493,'');
            }
        }else{
            $this->createResponse(401,'');
        }
    }

    private function StudentGetByCourse(){
        $this->requireUserSession();

        $rData = $this->requireRequestParams(array("Coursescheduleid"));

        $urlData = array(
            "roakIdno" => $rData->Coursescheduleid,
            "stagUser" => $_SESSION["stagUserOsCislo"]
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."student/getStudentiByRoakce?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();
            $sArr = $body[0]->studentPredmetu;

            if(sizeof($sArr) > 0){
                foreach ($sArr as $mst) {
                    $rs = array();

                    $rs["stagUserName"] = 	        $mst->userName;         
                    $rs["stagUserOsCislo"] = 	    $mst->osCislo;        
                    $rs["stagUserFirstName"] = 	    $mst->jmeno;         
                    $rs["stagUserLastName"] = 	    $mst->prijmeni;        
                    $rs["stagUserField"] = 	        $mst->nazevSp;    
                    $rs["stagUserFaculty"] =        $mst->fakultaSp;
                    $rs["stagUserCurrentGrade"] =   intval($mst->rocnik); 

                    array_push($retData,$rs);
                }
                
                $this->createResponse(200,$this->toJson($retData));
            }else{
                $this->createResponse(204,'');
            }
        }else{
            $this->createResponse(401,'');
        }
    }

    private function StudentGetByTerm(){
        $this->requireUserSession();

        $rData = $this->requireRequestParams(array("Termid"));

        $urlData = array(
            "termIdno" => $rData->Termid,
            "stagUser" => $_SESSION["stagUserOsCislo"]
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."student/getStudentiByTermin?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();
            $sArr = $body[0]->studentTerminu;

            if(sizeof($sArr) > 0){
                foreach ($sArr as $mst) {
                    $rs = array();

                    $rs["stagUserName"] = 	        $mst->userName;         
                    $rs["stagUserOsCislo"] = 	    $mst->osCislo;        
                    $rs["stagUserFirstName"] = 	    $mst->jmeno;         
                    $rs["stagUserLastName"] = 	    $mst->prijmeni;        
                    $rs["stagUserField"] = 	        $mst->nazevSp;    
                    $rs["stagUserFaculty"] =        $mst->fakultaSp;
                    $rs["stagUserCurrentGrade"] =   intval($mst->rocnik); 

                    array_push($retData,$rs);
                }
                
                $this->createResponse(200,$this->toJson($retData));
            }else{
                $this->createResponse(204,'');
            }
        }else{
            $this->createResponse(401,'');
        }

    }

    //? Teacher methods

    private function TeacherGetByName(){
        $this->requireUserSession();
        $rData = $this->requireRequestParams(array("Name","Surname"));

        $fnm = urldecode($rData->Name)."%";
        $lnm = urldecode($rData->Surname)."%";

        $urlData = array(
            "prijmeni" => urlencode($lnm),
            "jmeno" => urlencode($fnm),
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."ucitel/najdiUcitelePodleJmena?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;
        
        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();
            $tArr = $body[0]->ucitel;

            if(sizeof($tArr) > 0){
                foreach ($tArr as $mtc) {
                    $rt = array();
                
				    $rt["stagUserTeacherId"] =      $mtc->ucitIdno; 
				    $rt["stagUserFirstName"] =      $mtc->jmeno;
                    $rt["stagUserLastName"] =       $mtc->prijmeni;

                    array_push($retData,$rt);
                }
                
                $this->createResponse(200,$this->toJson($retData));
            }else{
                $this->createResponse(204,$this->toJson($retData));
            }

        }else{
            $this->createResponse(401,'');
        }
    }

    private function TeacherGetSchedule(){
        $this->requireUserSession();
        $rData = $this->requireRequestParams(array("Teacherid"));

        $OnlyUpcoming = ($this->existsAndValue("Onlyupcoming")[0] && $this->existsAndValue("Onlyupcoming")[1] === "true");
        $NoCreditTerms = (!$OnlyUpcoming && $this->existsAndValue("Nocreditterms")[0] && $this->existsAndValue("Nocreditterms")[1] === "true");
        $isDateFunc = (!$NoCreditTerms && !$OnlyUpcoming && $this->existsAndValue("Datefrom")[0] && $this->existsAndValue("Dateto")[0]);


        $urlData = array(
            "ucitIdno" => $rData->Teacherid,
            "stagUser" => $_SESSION["stagUserOsCislo"],
            "semestr" => $this->getCurrentSemester(),
            "rok" => date("Y") - 1,
            "jenRozvrhoveAkce" => ($NoCreditTerms ? "true" : "false"),
            "jenBudouciAkce" => ($OnlyUpcoming ? "true" : "false")
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        if($isDateFunc){
            $dFrom = $this->request["Datefrom"];
            $dTo = $this->request["Dateto"];
            if(DateTime::createFromFormat('d.m.Y',$dFrom) !== false){
                if(DateTime::createFromFormat('d.m.Y',$dTo) !== false){
                    $urlData["datumOd"] = $dFrom;
                    $urlData["datumDo"] = $dTo;
                }else{
                    $this->createResponse(494,'');
                }
            }else{
                $this->createResponse(494,'');
            }
        }

        $url = $this->defaultRequestUrl."rozvrhy/getRozvrhByUcitel?outputFormat=json";
        
        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        Unirest\Request::cookie($_SESSION['stagUserCookie']);
        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;
        
        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $retData = array();
            $sArr = $body[0]->rozvrhovaAkce;

            if(sizeof($sArr) > 0){
                foreach ($sArr as $sch) {
                    $rs = array();

                    $rs["CourseScheduleId"] =   $sch->roakIdno;

                    $rs["ScheduleId"]   =       $sch->predmet;
                    $rs["ScheduleName"] =       $sch->nazev;
                    $rs["ScheduleType"] =       $sch->typAkceZkr;

                    if($sch->datum->value == null || $sch->datum->value == "null"){
                        $rs["ScheduleDate"] =       $this->getDateByDay($sch->denZkr);
                        $rs["ScheduleStatic"] =       true;
                    }else{
                        $rs["ScheduleDate"] =       $sch->datum->value;
                    }

                    if(!$isDateFunc){
                        if($sch->tydenZkr == "J"){
                            continue;
                        }
                    }

                    $rs["ScheduleYear"] =       $sch->rok;

                    if($sch->denZkr == null || $sch->denZkr == "null"){
                        $rs["ScheduleDay"] =        $this->getDayByDate($rs["ScheduleDate"]);
                    }else{
                        $rs["ScheduleDay"] =        $sch->denZkr;
                    }

                    if($sch->hodinaSkutOd->value == "00:00"){
                        if($this->allowInvalidTimes){
                            $rs["ScheduleCourseEmptyTimeData"] = true;
                            $rs["ScheduleFrom"] = "07:00";
                            $rs["ScheduleTo"]   = "08:00";
                        }else{
                            continue;
                        }
                    }else{
                        $rs["ScheduleFrom"] =       $sch->hodinaSkutOd->value;
                        $rs["ScheduleTo"]   =       $sch->hodinaSkutDo->value;
                    }


                    $tch = array();
                    $tch["stagUserTeacherId"] = $sch->ucitel->ucitIdno; 
				    $tch["stagUserFirstName"] = $sch->ucitel->jmeno;
                    $tch["stagUserLastName"] =  $sch->ucitel->prijmeni;
                    $rs["ScheduleTeacher"]   =  $tch;

                    $rs["ScheduleDepartment"] = $sch->katedra;

                    if($sch->budova == null || $sch->budova == "null"){
                        // Find room by teacher???
                    }

					$rs["ScheduleBuilding"]  =  $sch->budova;
					$rs["ScheduleRoom"] =       $sch->mistnost;
					

                    array_push($retData,$rs);
                }
                
                $this->createResponse(200,$this->toJson($retData));
            }else{
                $this->createResponse(204,$this->toJson($retData));
            }
        }else{
            $this->createResponse(401,'');
        }
    }

    //? Room methods

    private function RoomGetFreeRoomsInBuilding(){
        $this->requireUserSession();
        $rData = $this->requireRequestParams(array("Schedulepos","Datefrom","Buildings"));

        $DateFrom = $rData->Datefrom;
        $SchedulePosition = $rData->Schedulepos;
        $BuildingList = json_decode(urldecode($rData->Buildings));

        $RoomList = array();
        foreach ($BuildingList as $building) {
            $res = $this->RoomGetAll($building);
            if($res != -1){
                $RoomList = array_merge($RoomList,$res);
            }else{
                $this->createResponse(406,'');
            } 
        }

        //print_r($RoomList);

        $RetRoomList = array();
        foreach ($RoomList as $room) {
            $res = $this->RoomCheckFree($room,$SchedulePosition,$DateFrom);
            if($res != -1){
                array_push($RetRoomList,$res);
            }else{
                $this->createResponse(406,'');
            } 
        }

        //print_r($RetRoomList);
        
        $this->createResponse(200,$this->toJson($RetRoomList));

    }

    private function RoomGetAll($building = ""){
        if(!$this->isUserLogged() || $building == "") return -1;

        $urlData = array(
            "zkrBudovy" => $building,
            "typ" => 2,
            "cisloMistnosti" => "%25",
            "pracoviste" => "%25"
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."mistnost/getMistnostiInfo?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
        $response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $rArr = $body[0]->mistnostInfo;
            $retData = array();
            if(sizeof($rArr) > 0){
                foreach ($rArr as $rm) {
                    $rs = array();

                    $rs["RoomFaculty"]  = $rm->katedra;
                    $rs["RoomBuilding"] = $rm->zkrBudovy;
                    $rs["RoomBuildingAdress"] = $rm->adresaBudovy;
                    $rs["RoomNumber"]   = $rm->cisloMistnosti;
                    $rs["RoomCapacity"] = $rm->kapacita;
                    $rs["RoomFloor"]    = $rm->podlazi;
                    
                    array_push($retData,$rs);
                }
                return $retData;
            }else{
                return -1;
            }
        }else{
            return -1;
        }

    }

    private function RoomCheckFree($room = null,$position = -1, $date = ""){
        if(!$this->isUserLogged() || $room == null || $position == -1 || $date == "") return -1;
        if(DateTime::createFromFormat('d.m.Y',$date) === false) return -1;

        $urlData = array(
            "stagUser" => $_SESSION["stagUserOsCislo"],
            "semestr" => $this->getCurrentSemester(),
            "datumOd" => $date,
            "datumDo" => $date,
            "mistnost" => $room["RoomNumber"],
            "budova" => $room["RoomBuilding"]
        );

        $url = $this->defaultRequestUrl."rozvrhy/getRozvrhByMistnost?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
        $response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;
        

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $rArr = $body[0]->rozvrhovaAkce;
            

            $roomFree = true;
            if(sizeof($rArr) > 0){
                foreach ($rArr as $sch) {
                    if($sch->hodinaSkutOd->value == "00:00"){
                        continue;
                    }else{
                        if($sch->hodinaOd -1 == $position){
                            $roomFree = false;
                        }
                    }
                }
                $room["RoomFree"] = $roomFree;
                return $room;
            }else{
                $room["RoomFree"] = true;
                return $room;
            }
        }else{
            return -1;
        }

    }

    //? Course methods

    private function CourseGetInfo($facultyId,$courseId){
        if($facultyId == null || $facultyId == "" || $courseId == null || $courseId == "") return -1;

        $urlData = array(
            "katedra" => $facultyId,
            "zkratka" => $courseId
        );

        $isEng = $this->existsAndValue("Eng")[0];
        if ($isEng) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."predmety/getPredmetInfo?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
        $response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $rm = $body[0];
            $retData = array();
            
            $retData["CourseFaculty"] = $rm->katedra;
            $retData["CourseId"] = $rm->zkratka;
            $retData["CourseName"] = $rm->nazev;
            $retData["CourseCredits"] = $rm->kreditu;
            $retData["CourseHasExam"] = ($rm->maZapocetPredZk == "NE" ? false : true); //Acredation Exam
            

            /* Get all teachers?? */



            return $retData;
        }else{
            return -1;
        }
    }

    private function CoursesGetByFaculty() {
        $rData = $this->requireRequestParams(array("Faculty"));

        $OnlyUpcoming = ($this->existsAndValue("Onlyupcoming")[0] && $this->existsAndValue("Onlyupcoming")[1] === "true");

        $isMini = $this->existsAndValue("Mini")[0];
        $isEng = $this->existsAndValue("Eng")[0];

        $urlData = array(
            "fakulta" => $rData->Faculty
        );

        if($this->existsAndValue("Year")[0]){
            $urlData["rok"] = $this->existsAndValue("Year")[1];
        }

        $url = $this->defaultRequestUrl."programy/getOboryQRAMInfo?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $crs = $body[0]->oborQRAMInfo;
            $retData = array();
            if(sizeof($crs) > 0){
                foreach ($crs as $cor) {
                    $rs = array();

                    if($isMini){
                        $rs["CourseId"] = $cor->oborIdno;
                        $rs["CourseName"] = ($isEng ? $cor->nazevEN : $cor->nazevCZ);   
                    }else{
                        $rs["CourseId"] = $cor->oborIdno;    
                        $rs["CourseNumber"] = $cor->cisloOboru;  
                        $rs["CourseCode"] = $cor->kodProgramu;  
                        $rs["CourseName"] = ($isEng ? $cor->nazevEN : $cor->nazevCZ);       
                        $rs["CourseShort"] = $cor->zkratka;
                        $rs["CourseType"] = $cor->forma;
                        $rs["CourseFaculty"] = $cor->fakultaOboru;
                    }
                    array_push($retData,$rs);
                }
                $this->createResponse(200,$this->toJson($retData));
            }else{
                $this->createResponse(200,$this->toJson($retData));
            }
        }else{
            $this->createResponse(401,'');
        }
    }

    private function CoursesGetSubjectsByCourseId() {
        $rData = $this->requireRequestParams(array("Subject"));

        $urlData = array(
            "oborIdno" => $rData->Subject
        );

        if(isset($this->request["Year"])){
            $urlData["rok"] = $this->request["Year"];
        }
        
        $showOptional = isset($this->request["Optional"]);

        $url = $this->defaultRequestUrl."predmety/getPredmetyByOborFullInfo?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $sub = $body[0]->predmetOboruFullInfo;
            $retData = array();
            if(sizeof($sub) > 0){
                foreach ($sub as $cor) {
                    $rs = array();

                    $rs["SubjectId"] = $cor->katedra."/".$cor->zkratka;    
                    $rs["SubjectName"] = $cor->nazev;
                    $rs["SubjectCredits"] = $cor->kreditu;
                    $rs["SubjectRecommendedYear"] = ($cor->doporucenyRocnik === null ? -1 : $cor->doporucenyRocnik);
                    $rs["SubjectRecommendedSemester"] = ($cor->doporucenySemestr === null ? "Any" : $cor->doporucenySemestr);
                    $rs["SubjectConditionalSubjects"] = [];

                    if($cor->podminujiciPredmety != ""){
                        $noSpaces = str_replace(' ','', $cor->podminujiciPredmety);
                        $rs["SubjectConditionalSubjects"] = explode(',',$noSpaces);
                    }

                    if(!$showOptional && $rs["SubjectRecommendedYear"] === -1){
                        continue;
                    }

                    array_push($retData,$rs);
                }
                $this->createResponse(200,$this->toJson($retData));
            }else{
                $this->createResponse(200,$this->toJson($retData));
            }
            
        }else{
            $this->createResponse(401,'');
        }
    }

    private function CoursesGetSubjectById() {
        $rData = $this->requireRequestParams(array("Faculty", "Id"));

        $urlData = array(
            "katedra" => $rData->Faculty,
            "zkratka" => $rData->Id
        );

        if(isset($this->request["Year"])){
            $urlData["rok"] = $this->request["Year"];
        }

        $url = $this->defaultRequestUrl."predmety/getPredmetInfo?outputFormat=json";

        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
		$body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if($status != 401){
            $cor = $body[0];
            if($cor != null){
                $rs = array();

                $rs["SubjectId"] = $cor->katedra."/".$cor->zkratka;    
                $rs["SubjectName"] = $cor->nazev;
                $rs["SubjectCredits"] = $cor->kreditu;
                $rs["SubjectRecommendedYear"] = ($cor->doporucenyRocnik === null ? -1 : $cor->doporucenyRocnik);
                $rs["SubjectRecommendedSemester"] = ($cor->doporucenySemestr === null ? "Any" : $cor->doporucenySemestr);
                $rs["SubjectConditionalSubjects"] = [];

                if($cor->podminujiciPredmety != ""){
                    $noSpaces = str_replace(' ','', $cor->podminujiciPredmety);
                    $rs["SubjectConditionalSubjects"] = explode(',',$noSpaces);
                }

                $this->createResponse(200,$this->toJson($rs));
            }else{
                $this->createResponse(200,'[]');
            }
        }else{
            $this->createResponse(401,'');
        }
    }

    private function CoursesGetSubjectByBlock() {
        $rData = $this->requireRequestParams(array("Blockid"));

        $urlData = array(
            "blokIdno" => $rData->Blockid
        );

        if ($this->existsAndValue("Eng")[0]) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."predmety/getPredmetyByBlok?outputFormat=json";
        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if ($status != 401) {
            $bSubjects = $body[0]->predmetBloku;
            $retData = array();

            if (sizeof($bSubjects) > 0) {
                foreach ($bSubjects as $sub) {
                    $ra = array();

                    $ra["SubjectName"] = $sub->nazev;
                    $ra["SubjectDepartment"] = $sub->katedra;
                    $ra["SubjectCode"] = $sub->zkratka;
                    $ra["SubjectCredits"] = $sub->kreditu;
                    $ra["SubjectYear"] = $sub->doporucenyRocnik;
                    $ra["SubjectSemester"] = $sub->doporucenySemestr;

                    array_push($retData,$ra);
                }
                $this->createResponse(200,$this->toJson($retData));
            } else {
                $this->createResponse(200,$this->toJson($retData));
            }
        } else {
            $this->createResponse(401,'');
        }
    }

    private function CoursesGetSubjectByBlockFull() {
        $rData = $this->requireRequestParams(array("Blockid"));

        $urlData = array(
            "blokIdno" => $rData->Blockid
        );

        if ($this->existsAndValue("Eng")[0]) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."predmety/getPredmetyByBlokFullInfo?outputFormat=JSON";
        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if ($status != 401) {
            $bSubjects = $body[0]->predmetBlokuFullInfo;
            $retData = array();

            if (sizeof($bSubjects) > 0) {
                foreach ($bSubjects as $sub) {
                    $ra = array();

                    $ra["SubjectName"] = $sub->nazev;
                    $ra["SubjectDepartment"] = $sub->katedra;
                    $ra["SubjectCode"] = $sub->zkratka;
                    $ra["SubjectCredits"] = $sub->kreditu;
                    $ra["SubjectYear"] = $sub->doporucenyRocnik;
                    $ra["SubjectSemester"] = $sub->doporucenySemestr;

                    if($sub->podminujiciPredmety !== ""){
                        $ra["SubjectConditionalSubjects"] = $this->getAllSubjectsFromString($sub->podminujiciPredmety);
                    } else {
                        $ra["SubjectConditionalSubjects"] = [];
                    }

                    if ($sub->podminujePredmety !== "") {
                        $ra["SubjectConditionForSubjects"] = $this->getAllSubjectsFromString($sub->podminujePredmety);
                    } else {
                        $ra["SubjectConditionForSubjects"] = [];
                    }

                    array_push($retData,$ra);
                }
                $this->createResponse(200,$this->toJson($retData));
            } else {
                $this->createResponse(200,$this->toJson($retData));
            }
        } else {
            $this->createResponse(401,'');
        }
    }

    //? Program methods

    private function ProgramsGetStudyPrograms() {
        $rData = $this->requireRequestParams(array("Faculty"));

        $urlData = array(
            "kod" => "%25",
            "pouzePlatne" => "true",
            "forma" => "P",
            "fakulta" => $rData->Faculty
        );

        if ($this->existsAndValue("Eng")[0]) {
            $urlData["lang"] = "en";
        }

        if ($this->existsAndValue("Year")[0]){
            $urlData["rok"] = $this->existsAndValue("Year")[1];
        }

        $url = $this->defaultRequestUrl."programy/getStudijniProgramy?outputFormat=json";
        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;
        
        $status = intval(end(explode(" ",$headers["0"])));
        if ($status != 401) {
            $cPrograms = $body[0]->programInfo;
            $retData = array();

            if (sizeof($cPrograms) > 0) {
                foreach ($cPrograms as $prg) {
                    $ra = array();

                    $ra["ProgramId"] = $prg->stprIdno;
                    $ra["ProgramName"] = $prg->nazev;
                    $ra["ProgramCode"] = $prg->kod;
                    $ra["ProgramTitle"] = $prg->titul;

                    array_push($retData,$ra);
                }
                $this->createResponse(200,$this->toJson($retData));
            } else {
                $this->createResponse(200,$this->toJson($retData));
            }
        } else {
            $this->createResponse(401,'');
        }
    }

    private function ProgramsGetStudyProgramFields() {
        $rData = $this->requireRequestParams(array("Programid"));

        $urlData = array(
            "stprIdno" => $rData->Programid
        );

        if ($this->existsAndValue("Eng")[0]) {
            $urlData["lang"] = "en";
        }

        if ($this->existsAndValue("Year")[0]){
            $urlData["rok"] = $this->existsAndValue("Year")[1];
        }

        $url = $this->defaultRequestUrl."programy/getOboryStudijnihoProgramu?outputFormat=json";
        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if ($status != 401) {
            $pFields = $body[0]->oborInfo;
            $retData = array();

            if (sizeof($pFields) > 0) {
                foreach ($pFields as $fld) {
                    $ra = array();

                    $ra["FieldId"] = $fld->oborIdno;
                    $ra["FieldName"] = $fld->nazev;
                    $ra["FieldCode"] = $fld->cisloOboru;

                    array_push($retData,$ra);
                }
                $this->createResponse(200,$this->toJson($retData));
            } else {
                $this->createResponse(200,$this->toJson($retData));
            }
        } else {
            $this->createResponse(401,'');
        }
    }

    private function ProgramsGetFieldPlans() {
        $rData = $this->requireRequestParams(array("Fieldid"));

        $urlData = array(
            "oborIdno" => $rData->Fieldid
        );

        if ($this->existsAndValue("Eng")[0]) {
            $urlData["lang"] = "en";
        }

        if ($this->existsAndValue("Year")[0]){
            $urlData["rok"] = $this->existsAndValue("Year")[1];
        }

        $url = $this->defaultRequestUrl."programy/getPlanyOboru?outputFormat=json";
        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if ($status != 401) {
            $fPlans = $body[0]->planInfo;
            $retData = array();

            if (sizeof($fPlans) > 0) {
                foreach ($fPlans as $pln) {
                    $ra = array();

                    $ra["PlanId"] = $pln->stplIdno;
                    $ra["PlanFieldId"] = $pln->oborIdno;
                    $ra["PlanName"] = $pln->nazev;
                    $ra["PlanVersion"] = $pln->verze;
                    $ra["PlanYearValidity"] = $pln->rokPlatnosti;

                    array_push($retData,$ra);
                }
                $this->createResponse(200,$this->toJson($retData));
            } else {
                $this->createResponse(200,$this->toJson($retData));
            }
        } else {
            $this->createResponse(401,'');
        }
    }

    private function ProgramsGetPlanSegments() {
        $rData = $this->requireRequestParams(array("Planid"));

        $withBlocks = $this->existsAndValue("Blocks")[0];

        $urlData = array(
            "stplIdno" => $rData->Planid
        );

        if ($this->existsAndValue("Eng")[0]) {
            $urlData["lang"] = "en";
        }

        if ($this->existsAndValue("Year")[0]){
            $urlData["rok"] = $this->existsAndValue("Year")[1];
        }

        $url = $this->defaultRequestUrl."programy/getSegmentyPlanu?outputFormat=json";
        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if ($status != 401) {
            $pSegments = $body[0]->segmentInfo;
            $retData = array();

            if (sizeof($pSegments) > 0) {
                foreach ($pSegments as $sgm) {
                    $ra = array();

                    $ra["SegmentId"] = $sgm->sespIdno;
                    $ra["SegmentName"] = $sgm->nazevModulu;
                    $ra["SegmentCode"] = $sgm->zkratka;
                    $ra["SegmentYearValidity"] = $sgm->rokPlatnosti;

                    if ($withBlocks) {
                        $segmentBlocks = $this->ProgramsGetSegmentBlocks($sgm->sespIdno);
                        if ($segmentBlocks !== null) {
                            $ra["SegmentBlocks"] = $segmentBlocks;
                        }
                    }

                    array_push($retData,$ra);
                }
                $this->createResponse(200,$this->toJson($retData));
            } else {
                $this->createResponse(200,$this->toJson($retData));
            }
        } else {
            $this->createResponse(401,'');
        }
    }

    private function ProgramsGetSegmentBlocks($segmentId = null) {
        if ($segmentId === null) {
            $rData = $this->requireRequestParams(array("Segmentid"));
        }

        $urlData = array(
            "sespIdno" => ($segmentId === null ? $rData->Segmentid : $segmentId)
        );

        if ($this->existsAndValue("Eng")[0]) {
            $urlData["lang"] = "en";
        }

        $url = $this->defaultRequestUrl."programy/getBlokySegmentu?outputFormat=json";
        foreach ($urlData as $key => $value) {
            $url .= "&".$key."=".$value;
        }

        $headersArray = array('Accept' => 'application/json');
		$response = Unirest\Request::get($url, $headersArray, null);
		$headers = $response->headers;
        $body = $response->body;

        $status = intval(end(explode(" ",$headers["0"])));
        if ($status != 401) {
            $sBlocks = $body[0]->blokInfo;
            $retData = array();

            if (sizeof($sBlocks) > 0) {
                foreach ($sBlocks as $blk) {
                    $ra = array();

                    $ra["BlockId"] = $blk->blokIdno;
                    $ra["BlockSegmentId"] = $blk->sespIdno;
                    $ra["BlockName"] = $blk->nazev;
                    $ra["BlockYearValidity"] = $blk->rokPlatnosti;

                    array_push($retData,$ra);
                }
                if ($segmentId === null) {
                    $this->createResponse(200,$this->toJson($retData));
                } else {
                    return $retData;
                }
                
            } else {
                if ($segmentId === null) {
                    $this->createResponse(200,$this->toJson($retData));
                } else {
                    return $retData;
                }
            }
        } else {
            if ($segmentId === null) {
                $this->createResponse(401,'');
            } else {
                return null;
            }
        }
    }

    //? Hack methods
    private function HackDecinRepresentativeVoting() {
        $client = new Client();
        $crawler = $client->request('GET', 'https://data.mmdecin.cz/otevrena-data/hlasovani-zastupitelstva/');

        $linkArray = array();
        $crawler->filter('.docman_categories > .docman_category .whitespace_preserver > .koowa_header__link')->each(function($node) use(&$linkArray, $client) {
            $link = $node->link();
            $innerCrawler = $client->click($link);
            $string = preg_replace('/\s+/', '', $node->text());
            $innerArray = array();
            $innerCrawler->filter('.docman_list_layout > form > .docman_document .docman_download__button.docman_track_download')->each(function($innerNode) use(&$innerArray) {
                $innerLink = $innerNode->link();
                array_push($innerArray, $innerLink->getUri());
            });
            $linkArray[$string] = $innerArray;
        });

        $this->createResponse(200,$this->toJson($linkArray));
    }

    private function HackDecinGetFiles() {
        $client = new Client();
        $crawler = $client->request('GET', 'https://data.mmdecin.cz/otevrena-data/hlasovani-zastupitelstva/');

        $linkArray = array();
        $crawler->filter('.docman_categories > .docman_category .whitespace_preserver > .koowa_header__link')->each(function($node) use(&$linkArray, $client) {
            $link = $node->link();
            $innerCrawler = $client->click($link);
            $string = preg_replace('/\s+/', '', $node->text());
            $innerArray = array();
            $innerCrawler->filter('.docman_list_layout > form > .docman_document')->each(function($docmanDiv) use(&$innerArray) {
                /*$metaDataArray = array();
                
                $docmanDiv->filter('.docman_description td')->each(function($meta) use(&$metaDataArray) {
                    array_push($metaDataArray, $meta->text());
                });*/

                $linkName = $docmanDiv->filter('.koowa_header .koowa_header__title_link  > span')->text();
                $linkText = $docmanDiv->filter('.docman_download > .docman_download__button.docman_track_download')->link()->getUri();
                $linkYear = explode('/', $linkText)[count(explode('/', $linkText))-3];
                $linkTextId = explode('/', $linkText)[count(explode('/', $linkText))-2];
                $linkId = explode('-', $linkTextId)[0];
                

                $retArr = array(
                    //'metaArray' => $metaDataArray,
                    'name' => $linkName,
                    'code' => $linkYear.'_'.$linkId,
                );

                array_push($innerArray, $retArr);
            });
            $linkArray[$string] = $innerArray;
        });

        $this->createResponse(200,$this->toJson($linkArray));
    }

    private function HackDecinGetVotesById() {
        $rData = $this->requireRequestParams(array("Dataid")); // Dataid = "rok_souborId_-_rok_souborId"
        $client = new Client();
        $crawler = $client->request('GET', 'https://data.mmdecin.cz/otevrena-data/hlasovani-zastupitelstva/');

        $dataIds = $rData->Dataid;
        $dataIds = explode('_-_', $dataIds); // array('rok_souborId', 'rok_souborId');
        $linkArray = array();

        $crawler->filter('.docman_categories > .docman_category .whitespace_preserver > .koowa_header__link')->each(function($node) use(&$linkArray, $client, $dataIds) {
            $link = $node->link();
            $innerCrawler = $client->click($link);
            $innerCrawler->filter('.docman_list_layout > form > .docman_document')->each(function($docmanDiv) use(&$linkArray, $dataIds) {
                $metaDataArray = array();
                
                $docmanDiv->filter('.docman_description td')->each(function($meta) use(&$metaDataArray) {
                    $text = $meta->text();
                    if (!ctype_space($text)){
                        array_push($metaDataArray, $meta->text());
                    }
                });

                $linkName = $docmanDiv->filter('.koowa_header .koowa_header__title_link  > span')->text();
                $linkText = $docmanDiv->filter('.docman_download > .docman_download__button.docman_track_download')->link()->getUri();
                $linkYear = explode('/', $linkText)[count(explode('/', $linkText))-3];
                $linkTextId = explode('/', $linkText)[count(explode('/', $linkText))-2];
                $linkId = explode('-', $linkTextId)[0];
                $linkCode = $linkYear.'_'.$linkId;

                foreach ($dataIds as $dataId) {
                    if($dataId === $linkCode) {
                        $retArr = array(
                            'metaArray' => $metaDataArray,
                            'url' => $linkText,
                        );
                        array_push($linkArray, $retArr);
                        break;
                    }
                }
            });
        });

        $retArr = array();

        foreach ($linkArray as $link) {
            $url = $link['url'];
            $source = file_get_contents($url);
            $coolSource = $this->autoUTF($source);
            $csvArray = array_map("createCSV", explode(";0", $coolSource));

            $linkYear = explode('/', $link['url'])[count(explode('/', $link['url']))-3];
            $linkTextId = explode('/', $link['url'])[count(explode('/', $link['url']))-2];
            $linkId = explode('-', $linkTextId)[0];
            $linkCode = $linkYear.'_'.$linkId;

            $resArr = array(
                'year' => explode('-', $linkYear)[0],
                'code' => $linkCode,
                'metadata' => $link['metaArray'],
                'data' => $csvArray,
            );

            array_push($retArr, $resArr);
        }

        $this->createResponse(200,$this->toJson($retArr));
    }

    //? Util methods

    private function isLoggedIn(){
		return isset($_SESSION['stagUserCookieExpire']);
    }
    
    private function isSessionExpired(){
        return ((time() > $_SESSION['stagUserCookieExpire'])?true:false);
    }

    private function requireRequestParams($paramArray) {
        $missingString = "Missing parameters:";
        $missing = false;
        $returnArray = array();

        foreach ($paramArray as $rParam) {
            if (!isset($this->request[$rParam])) {
                $missingString .= " " + $rParam;
                $missing = true;
            }else{
                $returnArray[$rParam] = $this->request[$rParam];
            }
        }

        if ($missing) {
            $this->createResponse(406,$missingString);
        } else {
            return (object) $returnArray;
        }
    }

    private function existsAndValue($varName) {
        return (isset($this->request[$varName]) ? array(true, $this->request[$varName]) : array(false, null));
    }

    private function canLogIn() {
        $canLogin = false;
        if($this->isLoggedIn()){
            if($this->isSessionExpired()){
                $canLogin = true;
            }else{
                $canLogin = false;
            }
        }else{
            $canLogin = true;
        }

        return $canLogin;
    }

    private function canLogOut($force) {
        $canLogout = false;
        if($this->isLoggedIn()){
            if($this->isSessionExpired()){
                session_unset(); 
			    session_destroy();
                $canLogout = false;
            }else{
                $canLogout = true;
            }
        }else{
            $canLogout = false;
        }

        return ($force ? $force : $canLogout);
    }

    private function requireUserSession() {
        if($this->isLoggedIn()){
            if($this->isSessionExpired()){
                $this->createResponse(401,'');
            }
        }else{
            $this->createResponse(401,'');
        }
    }
    
    private function isUserLogged() {
        if($this->isLoggedIn()){
            if($this->isSessionExpired()){
                return false;
            }
        }else{
            return false;
        }

        return true;
    }

    private function getCurrentSemester(){
        $cY = date("Y");
        $cM = date('n');
        $cD = date('j');

        $sD = $cY."-02-12";
        $eD = $cY."-09-24";

        $s = strtotime($sD);
        $e = strtotime($eD);
        $u = strtotime(date("Y-m-d"));


        if((($u >= $s) && ($u <= $e))){
            return "LS";
        }else{
            return "ZS";
        }
    }

    private function getDateByDay($day){
        $wcount = 0;
        switch($day){
            case "Po":
            $wcount = 0;
            break;
            case "Út":
            $wcount = 1;
            break;
            case "St":
            $wcount = 2;
            break;
            case "Čt":
            $wcount = 3;
            break;
            case "Pá":
            $wcount = 4;
            break;
            case "So":
            $wcount = 5;
            break;
            case "Ne":
            $wcount = 6;
            break;
        }

        return date('d.m.Y',strtotime('last monday +'.$wcount.' days', strtotime('tomorrow')));
    }

    private function getDayByDate($date){
        $rd = DateTime::createFromFormat('d.m.Y',$date);
        $d = $rd->format('D');
        $ret = "";
        switch($d){
            case "Mon":
            $ret = "Po";
            break;
            case "Tue":
            $ret = "Út";
            break;
            case "Wed":
            $ret = "St";
            break;
            case "Thu":
            $ret = "Čt";
            break;
            case "Fri":
            $ret = "Pá";
            break;
            case "Sat":
            $ret = "So";
            break;
            case "Sun":
            $ret = "Ne";
            break;
        }

        return $ret;
    }

    private function getAllSubjectsFromString($subjects) {
        $noSpaces = str_replace(' ','', $subjects);
        $subjectString = explode(',',$noSpaces);
        $rSubjectsObj = [];

        foreach ($subjectString as $rs) {
            $subjectExploded = explode('/', $rs);
            $newSubject = array(
                "SubjectDepartment" => $subjectExploded[0],
                "SubjectCode" => $subjectExploded[1],
            );
            array_push($rSubjectsObj, $newSubject);
        }

        return $rSubjectsObj;
    }

    private function toJson($data){
        if(is_array($data)){
            return json_encode($data);
        }
    }

    private function autoUTF($s) {
        // detect UTF-8
        if (preg_match('#[\x80-\x{1FF}\x{2000}-\x{3FFF}]#u', $s))
            return $s;

        // detect WINDOWS-1250
        if (preg_match('#[\x7F-\x9F\xBC]#', $s))
            return iconv('WINDOWS-1250', 'UTF-8', $s);

        // assume ISO-8859-2
        return iconv('ISO-8859-2', 'UTF-8', $s);
    }

    private function detect($s) {
        if (preg_match('#[\x80-\x{1FF}\x{2000}-\x{3FFF}]#u', $s))
            return 'UTF-8';

        if (preg_match('#[\x7F-\x9F\xBC]#', $s))
            return 'WINDOWS-1250';

        return 'ISO-8859-2';
    }
}

function createCSV($str) {
    $items = explode(';', $str);
    for ($i=0; $i < count($items); $i++) { 
        if ($i === 0) {
            $items[$i] = preg_replace('/\s+/S', "", $items[$i]);
        }
    }
    // Trim left and right
    return $items;
}
?>